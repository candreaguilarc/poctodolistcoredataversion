//
//  PresentSignUpViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class PresentSignUpAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toViewController = transitionContext.viewController(forKey: .to) as? SignUpViewController else {
                return
        }
        toViewController.view.backgroundColor = .clear
        toViewController.containerView.alpha = 0
        toViewController.gradientView.alpha = 0
        toViewController.loginButton.alpha = 0
        containerView.addSubview(toViewController.view)
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: duration, animations: {
            toViewController.containerView.alpha = 1.0
            toViewController.gradientView.alpha = 1.0
            toViewController.loginButton.alpha = 1.0
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.8
    }
    
}
