//
//  PresentCardViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

internal class PresentCardViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    internal var selectedCardFrame: CGRect = .zero
    internal var safeAreaWidth: CGFloat = 0.0
    internal var safeAreaHeight: CGFloat = 0.0
    internal var safeAreaTopInset: CGFloat = 0.0
    internal var safeAreaBottomInset: CGFloat = 0.0
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let toViewController = transitionContext.viewController(forKey: .to) as? CardViewController else {
                return
        }
        containerView.addSubview(toViewController.view)
        toViewController.view.backgroundColor = .clear
        toViewController.cardViewPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                     right: safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                     top: self.selectedCardFrame.origin.y,
                                                     bottom: safeAreaHeight - self.selectedCardFrame.origin.y - self.selectedCardFrame.height)
        
        toViewController.cardViewContainerPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                     right: safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                     top: self.selectedCardFrame.origin.y + safeAreaTopInset,
                                                     bottom: self.selectedCardFrame.origin.y + self.selectedCardFrame.height - self.safeAreaHeight - self.safeAreaBottomInset)
       
        toViewController.infoStackViewMoveDown()
        toViewController.iconImageViewMoveUp()
        toViewController.backButtonNotVisible()
        toViewController.newTaskButtonNotVisible()
        
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,  delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
            
            toViewController.cardViewPositionConstraints(left: 0.0,
                                                         right: 0.0,
                                                         top: 0.0,
                                                         bottom: 0.0)
            
            toViewController.cardViewContainerPositionConstraints(left: 0.0,
                                                         right: 0.0,
                                                         top: 0.0,
                                                         bottom: 0.0)
           
            toViewController.infoStackViewMoveUp()
            toViewController.iconImageViewMoveDown()
            toViewController.backButtonVisible()
            toViewController.newTaskButtonVisible()
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
}
