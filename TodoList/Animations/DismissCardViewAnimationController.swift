//
//  DismissCardViewAnimationController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/7/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import Foundation

import UIKit

internal class DismissCardViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    internal var selectedCardFrame: CGRect = .zero
    internal var safeAreaWidth: CGFloat = 0.0
    internal var safeAreaHeight: CGFloat = 0.0
    internal var safeAreaTopInset: CGFloat = 0.0
    internal var safeAreaBottomInset: CGFloat = 0.0
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        guard let fromViewController = transitionContext.viewController(forKey: .from) as? CardViewController,
            let toViewController = transitionContext.viewController(forKey: .to) as? POCBaseViewController else {
                return
        }
        containerView.insertSubview(toViewController.view, at: 0)
        let duration = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0, options: .curveLinear, animations: {
            fromViewController.cardViewPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                           right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                           top: self.selectedCardFrame.origin.y,
                                                           bottom: self.safeAreaHeight - self.selectedCardFrame.origin.y - self.selectedCardFrame.height)

            fromViewController.cardViewContainerPositionConstraints(left: self.selectedCardFrame.origin.x,
                                                                   right: self.safeAreaWidth - self.selectedCardFrame.origin.x - self.selectedCardFrame.width,
                                                                   top: self.selectedCardFrame.origin.y + self.safeAreaTopInset,
                                                                   bottom: self.selectedCardFrame.origin.y + self.selectedCardFrame.height - self.safeAreaHeight - self.safeAreaBottomInset)
            
            fromViewController.infoStackViewMoveDown()
            fromViewController.backButtonNotVisible()
            fromViewController.newTaskButtonNotVisible()
            fromViewController.iconImageViewMoveUp()
            fromViewController.view.backgroundColor = .clear
        }) { (_) in
            fromViewController.view.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
}
