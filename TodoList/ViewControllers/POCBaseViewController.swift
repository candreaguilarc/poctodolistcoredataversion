//
//  ViewController.swift
//  TodoList
//
//  Created by Carlo Andre Aguilar Castrat on 1/11/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class POCBaseViewController: UIViewController, DataDelegate{
    internal let presentCardAnimationController = PresentCardViewAnimationController()
    internal let dismissCardAnimationController = DismissCardViewAnimationController()
    
    var userRef: DatabaseReference?
    
    let segueIdentifier = "ExpandCardView"
    let collectionViewCellIdentifier = "CardCell"
    
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var notepadImageView: UIImageView!
    @IBOutlet weak var notepadImageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var notepadImageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var notepadImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cardCollectionView: UICollectionView!
    @IBOutlet weak var userImageView: CircleImageView!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var totalTasksLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var selectedTaskListIndex = 0
    var tools = Tools()
    
    var initialPositionFromSplashScreenNotepadImageViewConstraints: [NSLayoutConstraint] = []
    var initialPositionFromLoginSignUpScreensNotepadImageViewConstraints: [NSLayoutConstraint] = []
    var finalNotepadImageViewConstraints: [NSLayoutConstraint] = []
    
    private var indexOfCellBeforeDragging = 0
    private var collectionViewFlowLayout: UICollectionViewFlowLayout {
        return cardCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
    }
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDateLabel(date: Date())
        self.setTotalTasksLabel()
        self.setGreetinglabel()
        self.setAnimationConstraints()
        self.cardCollectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController
        {
            destinationViewController.delegate = self
            if let ppc = destinationViewController.popoverPresentationController
            {
                ppc.delegate = self
            }
        }
        if let destinationViewController = segue.destination as? CardViewController {
            destinationViewController.transitioningDelegate = self
            let indexPaths : Array = cardCollectionView.indexPathsForSelectedItems! as Array
            let indexPath : IndexPath = indexPaths[0]
            destinationViewController.delegate = self
            destinationViewController.selectedTaskListIndex = indexPath.row
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.configureCollectionViewLayoutItemSize()
    }
    
    //MARK: - Private
    
    func setAnimationConstraints(){
        initialPositionFromSplashScreenNotepadImageViewConstraints = [ self.notepadImageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor), self.notepadImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor), self.notepadImageView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.32)
        ]
        
        initialPositionFromLoginSignUpScreensNotepadImageViewConstraints = [ NSLayoutConstraint(
            item: self.notepadImageView, attribute: .centerY,
            relatedBy:.equal,
            toItem: self.view, attribute:.centerY,
            multiplier: 0.5, constant: 0), self.notepadImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor), self.notepadImageView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.25)
        ]
        
        finalNotepadImageViewConstraints = [ self.notepadImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20), self.notepadImageView.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 20), self.notepadImageView.heightAnchor.constraint(equalToConstant: 30)
        ]
    }
    
    func setDateLabel(date : Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        self.dateLabel.text = "Today: " + dateFormatter.string(from: date)
    }
    
    func setTotalTasksLabel(){
        var notCompletedTasksCounter = 0
        for taskList in UserDataManager.shared.allTaskLists{
            for task in taskList.tasks ?? NSOrderedSet() {
                if (task as? Task)?.value(forKey: "hasBeenCompleted") as? Bool == false {
                    notCompletedTasksCounter += 1
                }
            }
        }
        if notCompletedTasksCounter == 1 {
            self.totalTasksLabel.text = "You have 1 task to do"
        } else if notCompletedTasksCounter == 0 {
            self.totalTasksLabel.text = "All tasks are done!"
        }else {
            self.totalTasksLabel.text = "You have " + String(notCompletedTasksCounter) + " tasks to do"
        }
    }
    
    func setGreetinglabel() {
        self.greetingLabel.text = "Hello"
    }
    
    //MARK: - Public
    
    func updateCollectionView(){
        self.cardCollectionView.reloadData()
    }
    
    func changeGradientColors(){
        self.gradientView.endColor = UIColor.random
    }
    
    internal func notepadImageViewInitialAnimationPositionFromSplashScreen(){
        self.notepadImageViewTopConstraint.isActive = false
        self.notepadImageViewHeightConstraint.isActive = false
        self.notepadImageViewLeadingConstraint.isActive = false
        
        NSLayoutConstraint.activate(initialPositionFromSplashScreenNotepadImageViewConstraints)
        self.view.layoutIfNeeded()
    }
    
    internal func notepadImageViewInitialAnimationPositionFromLoginScreen(){
        self.notepadImageViewTopConstraint.isActive = false
        self.notepadImageViewHeightConstraint.isActive = false
        self.notepadImageViewLeadingConstraint.isActive = false
        
        NSLayoutConstraint.activate(initialPositionFromLoginSignUpScreensNotepadImageViewConstraints)
        self.view.layoutIfNeeded()
    }
    
    internal func notepadImageViewFinalAnimationPosition() {
        NSLayoutConstraint.activate(finalNotepadImageViewConstraints)
        self.view.layoutIfNeeded()
    }
}

extension POCBaseViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDataManager.shared.allTaskLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.collectionViewCellIdentifier, for: indexPath) as? CardCell else {
            return UICollectionViewCell()
        }
        cell.iconImageView.image = UIImage(named: UserDataManager.shared.allTaskLists[indexPath.row].value(forKey: "iconName") as? String ?? "")
        cell.listNameLabel.text = UserDataManager.shared.allTaskLists[indexPath.row].value(forKey: "listName") as? String
        self.tools.setTaskCountLabel(tasks: UserDataManager.shared.allTaskLists[indexPath.row].tasks?.array as? [Task] ?? [], label: cell.taskCountLabel)
        self.tools.setTasksProgress(tasks: UserDataManager.shared.allTaskLists[indexPath.row].tasks?.array as? [Task] ?? [], progressView: cell.taskListProgressView, progressLabel: cell.taskListProgressLabel)
        return cell
    }
    
    private func configureCollectionViewLayoutItemSize() {
        let inset: CGFloat = 40
        self.collectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        self.collectionViewFlowLayout.itemSize = CGSize(width: self.cardCollectionView.collectionViewLayout.collectionView!.frame.size.width - inset * 2, height: self.cardCollectionView.collectionViewLayout.collectionView!.frame.size.height - inset/2)
    }
    
    private func indexOfMajorCell() -> Int {
        let itemWidth = collectionViewFlowLayout.itemSize.width
        let proportionalOffset = self.cardCollectionView.collectionViewLayout.collectionView!.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = self.cardCollectionView.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.indexOfCellBeforeDragging = indexOfMajorCell()
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if self.indexOfCellBeforeDragging != IndexPath(row: self.indexOfMajorCell(), section: 0).row {
            self.changeGradientColors()
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = scrollView.contentOffset
        let indexOfMajorCell = self.indexOfMajorCell()
        let dataSourceCount = collectionView(self.cardCollectionView!, numberOfItemsInSection: 0)
        let swipeVelocityThreshold: CGFloat = 0.5
        let hasEnoughVelocityToSlideToTheNextCell = self.indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = self.indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        if didUseSwipeToSkipCell {
            self.changeGradientColors()
            let snapToIndex = self.indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = self.collectionViewFlowLayout.itemSize.width * CGFloat(snapToIndex)
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
        } else {
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            self.cardCollectionView.collectionViewLayout.collectionView!.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let attributes: UICollectionViewLayoutAttributes = self.cardCollectionView.layoutAttributesForItem(at: indexPath) {
            let cellRect: CGRect = attributes.frame
            let cellFrameInSuperview = self.cardCollectionView.convert(cellRect , to: self.cardCollectionView.superview)
            self.presentCardAnimationController.selectedCardFrame = cellFrameInSuperview
            self.dismissCardAnimationController.selectedCardFrame = cellFrameInSuperview
            self.presentCardAnimationController.safeAreaWidth = self.view.frame.width
            self.presentCardAnimationController.safeAreaHeight = self.view.frame.height - self.view.safeAreaInsets.bottom - self.view.safeAreaInsets.top
            self.dismissCardAnimationController.safeAreaWidth = self.view.frame.width
            self.dismissCardAnimationController.safeAreaHeight = self.view.frame.height - self.view.safeAreaInsets.bottom - self.view.safeAreaInsets.top
            self.presentCardAnimationController.safeAreaTopInset = self.view.safeAreaInsets.top
            self.presentCardAnimationController.safeAreaBottomInset = self.view.safeAreaInsets.bottom
            self.dismissCardAnimationController.safeAreaTopInset = self.view.safeAreaInsets.top
            self.dismissCardAnimationController.safeAreaBottomInset = self.view.safeAreaInsets.bottom
            self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
        }
    }
}

extension POCBaseViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.presentCardAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.dismissCardAnimationController
    }
}

extension POCBaseViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
