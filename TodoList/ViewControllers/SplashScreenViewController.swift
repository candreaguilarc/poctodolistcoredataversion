//
//  SplashScreenViewController.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/15/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase

class SplashScreenViewController: UIViewController {
    internal let splashScreenToLoginScreenAnimationController = SplashScreenToLoginScreenAnimationController()
    internal let splashScreenToHomeScreenAnimationController = SplashScreenToHomeScreenAnimationController()
    
    let loginSegueIdentifier = "SplashScreenToLogin"
    let homeSegueIdentifier = "SplashScreenToHome"
    
    @IBOutlet weak var notepadImageView: UIImageView!
    
    //MARK: - Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            UserDataManager.shared.setUserEmail()
            UserDataManager.shared.saveUsersHistory()
            UserDataManager.shared.addStore()
            UserDataManager.shared.retrieveData()
            if UserDataManager.shared.allTaskLists == [] {
                UserDataManager.shared.createDefaultTaskLists()
                self.performSegue(withIdentifier: self.homeSegueIdentifier, sender: nil)
            }
            else{
                self.performSegue(withIdentifier: self.homeSegueIdentifier, sender: nil)
            }
            
        } else {
            self.performSegue(withIdentifier: self.loginSegueIdentifier, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? LoginViewController {
            destinationViewController.transitioningDelegate = self
        } else if let destinationViewController = segue.destination as? POCBaseViewController {
            destinationViewController.transitioningDelegate = self
        }
    }
    
    //MARK: - Private
    
    internal func HideNotepadImageView(){
        self.notepadImageView.isHidden = true
    }
}

extension SplashScreenViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if let _ = presented as? LoginViewController {
            return self.splashScreenToLoginScreenAnimationController
        } else {
            return self.splashScreenToHomeScreenAnimationController
        }
    }
}
