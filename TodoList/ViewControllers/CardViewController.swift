//
//  cardViewController.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 1/29/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit
import Firebase
import CoreData

protocol DataDelegate: class {
    func updateCollectionView()
    func setTotalTasksLabel()
    func setDateLabel(date : Date)
}

class CardViewController: UIViewController, NewTaskDelegate {
    internal let presentNewTaskAnimationController = PresentNewTaskViewAnimationController()
    internal let dismissNewTaskAnimationController = DismissNewTaskViewAnimationController()
    let segueIdentifier = "PresentNewTaskView"
    let tableViewCellIdentifier = "TaskCell"
    
    @IBOutlet weak var newTaskButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tasksTableView: UITableView!
    @IBOutlet weak var cardViewContainer: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var taskCountLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var taskListProgressView: UIProgressView!
    @IBOutlet weak var taskListProgressLabel: UILabel!
    
    @IBOutlet weak var iconImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconImageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoStackViewLeadingConstraint: NSLayoutConstraint!
    
    var selectedTaskListIndex = 0
    var tools = Tools()
    weak var delegate: DataDelegate?
    var tasksRef: DatabaseReference?
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newTaskButton.layer.cornerRadius = self.newTaskButton.frame.size.height / 2
        self.tools.setTasksProgress(tasks: UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? [], progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        self.cardNameLabel.text = UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].value(forKey: "listName") as? String
        self.iconImageView.image = UIImage(named: UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].value(forKey: "iconName") as? String ?? "")
        self.tools.setTaskCountLabel(tasks: UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? [], label: taskCountLabel)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? NewTaskViewController{
            destinationViewController.transitioningDelegate = self
            destinationViewController.delegate = self
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func backButtonTouchUpInside(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
        self.delegate?.setTotalTasksLabel()
        self.delegate?.setDateLabel(date: Date())
    }
    
    @IBAction func newTaskButtonTouchUpInside(_ sender: Any) {
        self.performSegue(withIdentifier: self.segueIdentifier, sender: self)
    }
    
    //MARK: - Private
    
    func toggleCellCheckbox(_ cell: TaskCell, isCompleted: Bool) {
        if isCompleted {
            cell.accessoryType = .none
            cell.nameLabel?.textColor = .black
        } else {
            cell.accessoryType = .checkmark
            cell.nameLabel?.textColor = .gray
        }
    }
    
    internal func cardViewPositionConstraints(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.cardViewLeadingConstraint.constant = left
        self.cardViewTrailingConstraint.constant = right
        self.cardViewTopConstraint.constant = top
        self.cardViewBottomConstraint.constant = bottom
        self.view.layoutIfNeeded()
    }
    
    internal func cardViewContainerPositionConstraints(left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        self.cardViewContainerLeadingConstraint.constant = left
        self.cardViewContainerTrailingConstraint.constant = right
        self.cardViewContainerTopConstraint.constant = top
        self.cardViewContainerBottomConstraint.constant = bottom
        self.view.layoutIfNeeded()
    }
    
    internal func infoStackViewMoveDown(){
        self.infoStackViewTopConstraint.isActive = false
        self.infoStackViewBottomConstraint.isActive = true
        self.infoStackViewBottomConstraint.constant = 20
        self.infoStackViewLeadingConstraint.constant = 20
        self.infoStackViewTrailingConstraint.constant = 20
        self.view.layoutIfNeeded()
    }
    
    internal func infoStackViewMoveUp(){
        self.infoStackViewBottomConstraint.isActive = false
        self.infoStackViewTopConstraint.isActive = true
        self.infoStackViewTopConstraint.constant = 10
        self.infoStackViewLeadingConstraint.constant = 40
        self.infoStackViewTrailingConstraint.constant = 40
        self.view.layoutIfNeeded()
    }
    
    internal func iconImageViewMoveDown(){
        self.iconImageViewHeightConstraint =
            self.iconImageViewHeightConstraint.setMultiplier(multiplier: 1.8)
        self.iconImageViewTopConstraint.constant = 70
        self.view.layoutIfNeeded()
    }
    
    internal func iconImageViewMoveUp(){
        self.iconImageViewHeightConstraint =
            self.iconImageViewHeightConstraint.setMultiplier(multiplier: 2.3)
        self.iconImageViewTopConstraint.constant = 20
        self.view.layoutIfNeeded()
    }
    
    internal func backButtonNotVisible() {
        self.backButton.alpha = 0
    }
    
    internal func newTaskButtonNotVisible(){
        self.newTaskButton.alpha = 0
    }
    
    internal func newTaskButtonVisible(){
        self.newTaskButton.alpha = 1.0
    }
    
    internal func backButtonVisible() {
        self.backButton.alpha = 1.0
        
    }
    
    //MARK: - Public
    
    func addTask(taskName: String){
        UserDataManager.shared.insertTask(name: taskName, taskListIndex: self.selectedTaskListIndex)
        self.tasksTableView.reloadData()
        self.tools.setTaskCountLabel(tasks: UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? [], label: self.taskCountLabel)
        self.tools.setTasksProgress(tasks: UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? [], progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        self.delegate?.updateCollectionView()
    }
    
}

extension CardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.tableViewCellIdentifier, for: indexPath) as? TaskCell else {
            return UITableViewCell()
        }
        let tasks = UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task]
        cell.nameLabel.text = tasks?[indexPath.row].value(forKey: "name") as? String
        let task = tasks?[indexPath.row]
        if(task?.value(forKey: "hasBeenCompleted") as? Bool == false) {
            self.toggleCellCheckbox(cell, isCompleted: true)
        }else{
            self.toggleCellCheckbox(cell, isCompleted: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tasks = UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? []
        if tasks.count == 0 {
            self.tasksTableView.setEmptyMessage("You have no tasks yet")
        } else {
            self.tasksTableView.restore()
        }
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tasks = UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] 
        self.tasksTableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? TaskCell {
            let task = tasks?[indexPath.row]
            if(task?.value(forKey: "hasBeenCompleted") as? Bool == false) {
                self.toggleCellCheckbox(cell, isCompleted: false)
                tasks?[indexPath.row].setValue(true, forKey: "hasBeenCompleted")
            }else{
                self.toggleCellCheckbox(cell, isCompleted: true)
                tasks?[indexPath.row].setValue(false, forKey: "hasBeenCompleted")
            }
        }
        
        self.tools.setTasksProgress(tasks: tasks ?? [], progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
        self.tools.setTaskCountLabel(tasks: tasks ?? [], label: self.taskCountLabel)
        self.delegate?.updateCollectionView()
        UserDataManager.shared.saveContext()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?{
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            UserDataManager.shared.deleteTask(taskListIndex: self.selectedTaskListIndex, taskIndex: indexPath.row)
            self.tasksTableView.deleteRows(at: [indexPath], with: .fade)
            let tasks = UserDataManager.shared.allTaskLists[self.selectedTaskListIndex].tasks?.array as? [Task] ?? []
            self.tools.setTaskCountLabel(tasks: tasks, label: self.taskCountLabel)
            self.tools.setTasksProgress(tasks: tasks, progressView: self.taskListProgressView, progressLabel: self.taskListProgressLabel)
            self.delegate?.updateCollectionView()
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        return [deleteAction]
    }
}

extension CardViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.presentNewTaskAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.dismissNewTaskAnimationController
    }
}
