//
//  ColorExtension.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 2/25/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
