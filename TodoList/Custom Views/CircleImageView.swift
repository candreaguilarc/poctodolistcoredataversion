//
//  RoundImageView.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 1/17/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

@IBDesignable
class CircleImageView: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
}
