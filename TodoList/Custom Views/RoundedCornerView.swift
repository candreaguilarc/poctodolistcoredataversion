//
//  RoundedCornerView.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/3/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

class RoundedCornerView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
    }
}

