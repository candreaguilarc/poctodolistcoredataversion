//
//  TaskCell.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/1/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    //Mark: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
