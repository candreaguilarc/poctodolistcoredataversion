//
//  UserData.swift
//  POCTodoList
//
//  Created by Carlo Aguilar on 2/21/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//

import Foundation
import Firebase
import CoreData

class UserDataManager {
    static let shared = UserDataManager()
    
    let modelName = "DataModel"
    var userEmail = ""
    var allTaskLists: [TaskList] = []
    
    //MARK: - Private
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            fatalError("Unable to Find Data Model")
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }
        return managedObjectModel
    }()
    
    private lazy var persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    
    private(set) lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        return managedObjectContext
    }()
    
    func setUserEmail() {
        self.userEmail =  Auth.auth().currentUser?.email ?? ""
    }
    
    func saveUsersHistory(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let predicate = NSPredicate(format: "email = %@", self.userEmail)
        fetchRequest.predicate = predicate
        do{
            let result = try managedContext.fetch(fetchRequest)
            if result.count == 0 {
                let userEntity = NSEntityDescription.entity(forEntityName: "User", in: managedContext) ?? NSEntityDescription()
                let user = User(entity: userEntity, insertInto: managedContext)
                user.setValue(self.userEmail, forKey: "email")
                appDelegate.saveContext()
            }
        } catch {
            return
        }
    }
    
    func addStore(){
        let fileManager = FileManager.default
        let storeName = "\(self.userEmail).sqlite"
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        do {
            try self.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                                   configurationName: nil,
                                                                   at: persistentStoreURL,
                                                                   options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: false])
        } catch {
            fatalError("Unable to Load Persistent Store")
        }
    }
    
    func createDefaultTaskLists(){
        let taskListEntity = NSEntityDescription.entity(forEntityName: "TaskList", in: self.managedObjectContext) ?? NSEntityDescription()
        let taskEntity = NSEntityDescription.entity(forEntityName: "Task", in: self.managedObjectContext) ?? NSEntityDescription()
        
        let todayTaskList = TaskList(entity: taskListEntity, insertInto: self.managedObjectContext)
        todayTaskList.setValue("Today", forKey: "listName")
        todayTaskList.setValue("today", forKey: "iconName")
        
        let workTaskList = TaskList(entity: taskListEntity, insertInto: self.managedObjectContext)
        workTaskList.setValue("Work", forKey: "listName")
        workTaskList.setValue("work", forKey: "iconName")
        
        let personalTaskList = TaskList(entity: taskListEntity, insertInto: self.managedObjectContext)
        personalTaskList.setValue("Personal", forKey: "listName")
        personalTaskList.setValue("personal", forKey: "iconName")
        
        let task = Task(entity: taskEntity, insertInto: self.managedObjectContext)
        task.setValue("Install the TodoList app", forKey: "name")
        task.setValue(true, forKey: "hasBeenCompleted")
        todayTaskList.addToTasks(task)
        
        self.allTaskLists = [todayTaskList, workTaskList, personalTaskList]
        self.saveContext()
    }
    
    func insertTask(name: String, taskListIndex: Int){
        let taskEntity = NSEntityDescription.entity(forEntityName: "Task", in: self.managedObjectContext) ?? NSEntityDescription()
        let task = Task(entity: taskEntity, insertInto: self.managedObjectContext)
        task.setValue(name, forKey: "name")
        task.setValue(false, forKey: "hasBeenCompleted")
        self.allTaskLists[taskListIndex].addToTasks(task)
        self.saveContext()
    }
    
    func retrieveData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TaskList")
        let sortByName = NSSortDescriptor(key: "listName", ascending: true)
        let predicate = NSPredicate(format: "listName = %@", "Today")
        fetchRequest.predicate = predicate
        do{
            var result = try managedObjectContext.fetch(fetchRequest)
            if result.count != 0{
                let todayTaskList = result[0] as? TaskList ?? TaskList()
                self.allTaskLists.append(todayTaskList)
            }
            
            fetchRequest.predicate = NSPredicate(format: "NOT listName = %@", "Today")
            fetchRequest.sortDescriptors = [sortByName]
            
            result = try managedObjectContext.fetch(fetchRequest)
            if result.count != 0{
                let otherTasksLists = result as? [TaskList] ?? []
                self.allTaskLists.append(contentsOf: otherTasksLists)
            }
        } catch {
            return
        }
    }
    
    func deleteTask(taskListIndex: Int, taskIndex: Int){
        self.allTaskLists[taskListIndex].removeFromTasks(at: taskIndex)
        self.saveContext()
    }
    
    func deleteEverything(){
        let fetchRequest1 = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        do{
            let result = try self.managedObjectContext.fetch(fetchRequest1)
            for taskList in result{
                self.managedObjectContext.delete(taskList as! NSManagedObject)
            }
            self.saveContext()
        } catch {
            return
        }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TaskList")
        do{
            let result = try self.managedObjectContext.fetch(fetchRequest)
            for taskList in result{
                self.managedObjectContext.delete(taskList as! NSManagedObject)
            }
            self.saveContext()
        } catch {
            return
        }
    }
    
    func saveContext() {
        if managedObjectContext.hasChanges {
            do {
                try self.managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
