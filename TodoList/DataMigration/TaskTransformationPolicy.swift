//
//  Policy.swift
//  POCTodoList
//
//  Created by Carlo Andre Aguilar Castrat on 3/13/19.
//  Copyright © 2019 Belatrix. All rights reserved.
//
import CoreData

class TaskTransformationPolicy: NSEntityMigrationPolicy {
    
@objc
    func changeFromBoolToString(hasBeenCompleted: NSNumber) -> NSString {
        if hasBeenCompleted.boolValue == true {
            return "true"
        } else {
            return "false"
        }
    }
    
@objc
    func changeFromStringToBool(hasBeenCompleted: NSString) -> NSNumber {
        if hasBeenCompleted == "true" {
            return true
        } else {
            return false
        }
    }
}
